package com.realtouchapp.receiptsample;

import android.app.Application;
import android.util.Base64;

import com.wpx.WPXMain;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // 初始化蓝牙适配器
        WPXMain.init(this);

//        Log.e("test1", "prize-hex="+hexEncodedPayPrize(2000));
//        Log.e("test2", "aes-base64="+AES_Base64_EncodedMessage("AE-92681762", "6222"));

    }

    /**
     * 價格轉換成16進位字串，左側補0補滿8位
     *
     * @param prize 價格
     * @return 長度為8的16進位字串
     */
    private String hexEncodedPayPrize(int prize) {
        String ZEROES = "00000000";
        String s = Integer.toString(prize, 16);
        String intAsString = s.length() <= ZEROES.length() ? ZEROES.substring(s.length()) + s : s;
        return intAsString;
    }

    /**
     * 將發票字軌十碼及隨機碼四碼以字串方式合併後使用 AES 加密並採用 Base64 編碼轉換。
     *
     * @param number 發票號碼 (對獎用的那10碼，例如：AB11223344)
     * @param random 隨機碼 (參考，API 的回傳，例如：8111)
     * @return 長度為24的加密後的字串
     */
    private String AES_Base64_EncodedMessage(String number, String random) {
        // AES加密用的金鑰
        String AESKey = "12345678901234567890123456789012"; // for test

        // 把字串中的-全刪掉
        number = number.replaceAll("-", "");
        String msg = number+random;

        try
        {
            SecretKeySpec mSecretKeySpec = new SecretKeySpec(AESKey.getBytes("utf-8"), "AES");
            Cipher mCipher = Cipher.getInstance("AES");
            mCipher.init(Cipher.ENCRYPT_MODE, mSecretKeySpec);
            byte[] aesFinal = mCipher.doFinal(msg.getBytes("utf-8"));

            return Base64.encodeToString(aesFinal, Base64.NO_WRAP);
        }
        catch(Exception ex)
        {
            return null;
        }
    }

}
