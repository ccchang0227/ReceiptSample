package com.realtouchapp.receiptsample;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.smartdevice.sdk.printer.PrintService;
import com.smartdevice.sdk.printer.PrinterClass;
import com.wpx.IBluetoothPrint;
import com.wpx.WPXMain;
import com.wpx.util.GeneralAttributes;
import com.wpx.util.ZXingCodeCreate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;

public class BluetoothDeviceActivity extends Activity {
    // 是否為藍牙模式或測試頁模式
    public static final String BUNDLE_EXTRA_BLUETOOTH_MODE_KEY = "IsBluetoothMode";
    public static final String BUNDLE_EXTRA_DEVICE_KEY = "device";

    private boolean isBluetoothMode = false;

    private BluetoothDevice device = null;
    private String address = null;
    private TextView tv_name, tv_conn;

    private ImageView iv_receipt_image;
    private ImageView iv_ordered_menu_image;

    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_device);

        final boolean isInit;
        final Intent intent = getIntent();
        if (intent != null && intent.hasExtra(BUNDLE_EXTRA_DEVICE_KEY)) {
            device = intent.getParcelableExtra(BUNDLE_EXTRA_DEVICE_KEY);
            if (device != null) {
                final String name = device.getName();
                address = device.getAddress();
                // WPXMain.connectDevice(address);
                isInit = true;
            }
            else {
                isInit = false;
            }
        }
        else {
            isInit = false;
        }

        isBluetoothMode = false;
        if (intent != null && intent.hasExtra(BUNDLE_EXTRA_BLUETOOTH_MODE_KEY)) {
            isBluetoothMode = intent.getBooleanExtra(BUNDLE_EXTRA_BLUETOOTH_MODE_KEY, false);
        }

        if (!isInit && isBluetoothMode) {
            finish();
            return;
        }

        initView();
        initContents();

        if (isBluetoothMode && isInit) {
            // 我也不知道他為什麼要delay
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // 連接裝置
                    boolean isConntected = WPXMain.connectDevice(address);
                    Toast.makeText(getApplicationContext(),
                            isConntected ? R.string.connect_success : R.string.connect_fail,
                            Toast.LENGTH_SHORT).show();
                    tv_name.setText(device.getName());
                    tv_conn.setText(isConntected ? R.string.connect_success : R.string.connect_fail);
                    handler.removeCallbacks(this);
                }
            }, 500);
        }

    }

    @Override
    protected void onDestroy() {

        Drawable drawable = iv_receipt_image.getDrawable();
        if (drawable != null) {
            if (drawable instanceof BitmapDrawable) {
                Bitmap bm = ((BitmapDrawable) drawable).getBitmap();
                if (bm != null && !bm.isRecycled()) {
                    bm.recycle();
                }
            }
            iv_receipt_image.setImageDrawable(null);
        }

        drawable = iv_ordered_menu_image.getDrawable();
        if (drawable != null) {
            if (drawable instanceof BitmapDrawable) {
                Bitmap bm = ((BitmapDrawable) drawable).getBitmap();
                if (bm != null && !bm.isRecycled()) {
                    bm.recycle();
                }
            }
            iv_ordered_menu_image.setImageDrawable(null);
        }

        if (isBluetoothMode) {
            WPXMain.disconnectDevice();
        }

        super.onDestroy();
    }

    private void initView() {
        // 裝置名稱
        tv_name = (TextView) findViewById(R.id.tv_name);
        if (isBluetoothMode) {
            tv_name.setText(device.getAddress());
        }
        else {
            tv_name.setVisibility(View.GONE);
        }

        // 裝置狀態
        tv_conn = (TextView) findViewById(R.id.tv_conn);
        if (isBluetoothMode) {
            tv_conn.setText(R.string.connecting);
        }
        else {
            tv_conn.setVisibility(View.GONE);
        }

        // 退出
        View btn_exit = findViewById(R.id.btn_exit);
        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private int isShowIn = 0;

    private void initContents() {
        // 測試列印電子發票
        View btn_test_print_receipt = findViewById(R.id.btn_test_print_receipt);
        if (!isBluetoothMode) {
            btn_test_print_receipt.setVisibility(View.GONE);
        }
        btn_test_print_receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testPrintReceipt();
            }
        });

        // 測試電子發票圖片
        iv_receipt_image = (ImageView) findViewById(R.id.iv_receipt_image);

        View btn_test_receipt_image = findViewById(R.id.btn_test_receipt_image);
        btn_test_receipt_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Drawable drawable = iv_receipt_image.getDrawable();
                if (drawable != null) {
                    if (drawable instanceof BitmapDrawable) {
                        Bitmap bm = ((BitmapDrawable) drawable).getBitmap();
                        if (bm != null && !bm.isRecycled()) {
                            bm.recycle();
                        }
                    }
                    iv_receipt_image.setImageDrawable(null);
                }
                else {
                    Bitmap bm = createFullSampleReceiptImage();
                    iv_receipt_image.setImageBitmap(bm);
                }

            }
        });

        // --------------------------------------------------------------------

        // 測試列印出菜單 (外場+內場) (未撰寫)
        View btn_test_print_ordered_menu = findViewById(R.id.btn_test_print_ordered_menu);
        if (!isBluetoothMode) {
            btn_test_print_ordered_menu.setVisibility(View.GONE);
        }
        btn_test_print_ordered_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printTestOrderredMenu();
            }
        });

        // 測試出菜單圖片
        iv_ordered_menu_image = (ImageView) findViewById(R.id.iv_ordered_menu_image);

        // 產生出菜單圖片 (內場) (未完成)
        View btn_test_ordered_menu_in_image = findViewById(R.id.btn_test_ordered_menu_in_image);
        btn_test_ordered_menu_in_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Drawable drawable = iv_ordered_menu_image.getDrawable();
                if (drawable != null) {
                    if (drawable instanceof BitmapDrawable) {
                        Bitmap bm = ((BitmapDrawable) drawable).getBitmap();
                        if (bm != null && !bm.isRecycled()) {
                            bm.recycle();
                        }
                    }
                    iv_ordered_menu_image.setImageDrawable(null);
                }

                if (isShowIn == 1) {
                    isShowIn = 0;
                    return;
                }

                isShowIn = 1;
                Bitmap bm = createSampleOrderredMenuImage(true);
                iv_ordered_menu_image.setImageBitmap(bm);

            }
        });

        // 產生出菜單圖片 (外場) (未完全完成)
        View btn_test_ordered_menu_out_image = findViewById(R.id.btn_test_ordered_menu_out_image);
        btn_test_ordered_menu_out_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Drawable drawable = iv_ordered_menu_image.getDrawable();
                if (drawable != null) {
                    if (drawable instanceof BitmapDrawable) {
                        Bitmap bm = ((BitmapDrawable) drawable).getBitmap();
                        if (bm != null && !bm.isRecycled()) {
                            bm.recycle();
                        }
                    }
                    iv_ordered_menu_image.setImageDrawable(null);
                }

                if (isShowIn == 2) {
                    isShowIn = 0;
                    return;
                }

                isShowIn = 2;
                Bitmap bm = createSampleOrderredMenuImage(false);
                iv_ordered_menu_image.setImageBitmap(bm);

            }
        });


        View btn_test_open_box = findViewById(R.id.btn_test_open_box);
        btn_test_open_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WPXMain.printCommand(GeneralAttributes.INSTRUCTIONS_ESC_OPEN_BOX);
            }
        });
    }

    /** 列印電子發票範例 */
    int start = 0;
    private void testPrintReceipt() {
        // 測試藍牙打印機是否可以和wifi打印機同時列印 (結果: 可以)
//        printTestOrderredMenu();

        final IBluetoothPrint bp = WPXMain.getBluetoothPrint();

        WPXMain.printCommand(GeneralAttributes.INSTRUCTIONS_ESC_INIT);

        // 分段列印圖片 (不然會當)
        start = 0;
        handler.post(new Runnable() {

            @Override
            public void run() {
//                if (true) return;

                boolean delayNextPrint = true;

                final IBluetoothPrint.Describe des = new IBluetoothPrint.Describe();
                des.setGravity((byte) IBluetoothPrint.GRAVITY_CENTER);

                int maxWidth = 384; //570

                // 可以實際印一張出來比對資料區塊
                if (start == 0) {
                    // 印發票上方的資訊 (包括code39一維碼)
                    Bitmap bm = Bitmap.createBitmap(maxWidth, 380, Bitmap.Config.RGB_565);
                    Canvas c = new Canvas(bm);
                    c.drawRGB(255, 255, 255);

                    Paint tp = new Paint(Paint.ANTI_ALIAS_FLAG);
                    tp.setTextAlign(Paint.Align.CENTER);
                    tp.setColor(Color.BLACK);

                    tp.setTextSize(38);
                    float offY=0;
                    Paint.FontMetrics fm = tp.getFontMetrics();
                    float fontTotalHeight = fm.bottom - fm.top;
                    float offY1 = fontTotalHeight / 2 - fm.bottom;
                    float newY = offY + fontTotalHeight/2 + offY1;

                    String txt = "于江創意有限公司";
                    c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

                    offY = offY+fontTotalHeight-10;
                    tp.setTextSize(43);
                    tp.setFakeBoldText(false);
                    fm = tp.getFontMetrics();
                    fontTotalHeight = fm.bottom - fm.top;
                    offY1 = fontTotalHeight / 2 - fm.bottom;
                    newY = offY + fontTotalHeight/2 + offY1;
                    txt = "電子發票證明聯補印";
                    c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

                    offY = offY+fontTotalHeight-10;
                    tp.setTextSize(47);
                    tp.setFakeBoldText(true);
                    fm = tp.getFontMetrics();
                    fontTotalHeight = fm.bottom - fm.top;
                    offY1 = fontTotalHeight / 2 - fm.bottom;
                    newY = offY + fontTotalHeight/2 + offY1;
                    txt = "105年09-10月";
                    c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

                    offY = offY+fontTotalHeight-10;
                    newY = offY + fontTotalHeight/2 + offY1;
                    txt = "KT-28773338";
                    c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

                    offY = offY+fontTotalHeight-5;
                    //*
                    Log.e("TEST-Y-1", "offY="+offY); // 353.45117, 313.45117
                    tp.setTextSize(24);
                    tp.setFakeBoldText(false);
                    tp.setTextAlign(Paint.Align.LEFT);
                    fm = tp.getFontMetrics();
                    fontTotalHeight = fm.bottom - fm.top;
                    offY1 = fontTotalHeight / 2 - fm.bottom;
                    newY = offY + fontTotalHeight/2 + offY1;
                    txt = "2016/10/11 06:48:45";
                    c.drawText(txt, 0, txt.length(), 20, newY, tp);

                    offY = offY+fontTotalHeight-5;
                    newY = offY + fontTotalHeight/2 + offY1;
                    txt = "隨機碼: 2956";
                    c.drawText(txt, 0, txt.length(), 20, newY, tp);

                    txt = "總計: 990";
                    c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

                    offY = offY+fontTotalHeight-5;
                    newY = offY + fontTotalHeight/2 + offY1;
                    txt = "賣方: 10718853";
                    c.drawText(txt, 0, txt.length(), 20, newY, tp);

                    txt = "買方: 53796668";
                    c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

                    offY = offY+fontTotalHeight;
                    Log.e("TEST-Y-2", "offY="+offY); //495.88672, 425.88672
                    //*/
                    //*
                    offY = offY+20;
                    Bitmap d1code = ZXingCodeCreate.createOneDCode("10510LE422206938111", maxWidth, 80, false);
                    c.drawBitmap(d1code,
                            new Rect(0, 0, d1code.getWidth(), d1code.getHeight()),
                            new Rect(0, (int)offY, maxWidth, (int)(offY+80)),
                            new Paint(Paint.ANTI_ALIAS_FLAG));
                    d1code.recycle();

                    offY = offY+80;
                    Log.e("TEST-Y-3", "offY="+offY);
                    //*/

                    // 印外框
//                    Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
//                    p.setColor(Color.BLACK);
//                    p.setStrokeWidth(4);
//                    p.setStyle(Style.STROKE);
//                    c.drawRect(0, 0, 569, 484, p);

                    des.setType(IBluetoothPrint.TYPE_IMG);
                    des.setWidthP(1);
                    des.setHeightP(1);
                    bp.printBitmap(bm, des);
                    bm.recycle();

                    delayNextPrint = true;
                }
                else if (start == -1) { // 不執行
                    // 以印純文字的方式印部分資訊
                    des.setType(IBluetoothPrint.TYPE_TEXT);
                    des.setBlod(false);
                    des.setTextSize(IBluetoothPrint.TEXT_SIZE_S);
                    des.setGravity((byte) IBluetoothPrint.GRAVITY_LEFT);
                    bp.printText("2016-10-11 06:48:45\n隨機碼: 2956    總計: 990\n賣方: 10718853  買方: 53796668\n", des);

                    delayNextPrint = true;
                }
                else if (start == -2) { // 不執行
                    // 只印一維碼
                    Bitmap d1code = ZXingCodeCreate.createOneDCode("10510LE422206938111", maxWidth, 80, false);
                    des.setType(IBluetoothPrint.TYPE_IMG);
                    des.setWidthP(0.8f);
                    des.setHeightP(1);
                    des.setGravity((byte) IBluetoothPrint.GRAVITY_CENTER);
                    bp.printBitmap(d1code, des);
//                    bp.printOne("10510LE422206938111", des);
                    d1code.recycle();

                    delayNextPrint = true;
                }
                else if (start == 1) {
                    // 印下方的兩個QRCode
                    int width = maxWidth/2;

                    Bitmap bm = Bitmap.createBitmap(maxWidth, width-20, Bitmap.Config.RGB_565);
                    Canvas c = new Canvas(bm);
                    c.drawRGB(255, 255, 255);

//                    int codeWidth = width/4;
                    int codeWidth = 150;

                    String txt = "LE422206931050926811100000015000000160000000028426674yZ1zsLCd0XRVOg7VXOsI5A==:**********:5:5:1:24H光泉高鈣鮮豆漿:1:52:45 波的多蚵仔煎:1:16:";
                    try {
                        Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
                        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
                        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
                        hints.put(EncodeHintType.MARGIN, 0);
                        BitMatrix bitMatrix = new QRCodeWriter().encode(txt, BarcodeFormat.QR_CODE, codeWidth, codeWidth, hints);
                        int[] pixels = new int[codeWidth*codeWidth];
                        //下面這裡按照二維碼的算法，逐個生成二維碼的圖片，
                        //兩個for循環是圖片橫列掃描的結果
                        for (int y = 0; y < codeWidth; y++) {
                            for (int x = 0; x < codeWidth; x++) {
                                if (bitMatrix.get(x, y)) {
                                    pixels[y * codeWidth + x] = 0xff000000;
                                }
                                else {
                                    pixels[y * codeWidth + x] = 0xffffffff;
                                }
                            }
                        }

                        Bitmap d2code = Bitmap.createBitmap(pixels, codeWidth, codeWidth, Bitmap.Config.RGB_565);
                        c.drawBitmap(d2code,
                                new Rect(0, 0, d2code.getWidth(), d2code.getHeight()),
                                new Rect(10, 0, width-10, width-20),
                                new Paint(Paint.ANTI_ALIAS_FLAG));
                        d2code.recycle();

                    } catch (WriterException e) {
                        e.printStackTrace();
                    }

                    txt = "**45 波的多蚵仔煎:1:16:90 統一肉燥麵碗:1:19:68 統一肉骨茶碗:1:19";
//                    txt = "**";
                    try {
                        Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
                        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
                        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
                        hints.put(EncodeHintType.MARGIN, 0);
                        BitMatrix bitMatrix = new QRCodeWriter().encode(txt, BarcodeFormat.QR_CODE, codeWidth, codeWidth, hints);
                        int[] pixels = new int[codeWidth*codeWidth];
                        //下面這裡按照二維碼的算法，逐個生成二維碼的圖片，
                        //兩個for循環是圖片橫列掃描的結果
                        for (int y = 0; y < codeWidth; y++) {
                            for (int x = 0; x < codeWidth; x++) {
                                if (bitMatrix.get(x, y)) {
                                    pixels[y * codeWidth + x] = 0xff000000;
                                }
                                else {
                                    pixels[y * codeWidth + x] = 0xffffffff;
                                }
                            }
                        }

                        Bitmap d2code = Bitmap.createBitmap(pixels, codeWidth, codeWidth, Bitmap.Config.RGB_565);
                        c.drawBitmap(d2code,
                                new Rect(0, 0, d2code.getWidth(), d2code.getHeight()),
                                new Rect(width+10, 0, maxWidth-10, width-20),
                                new Paint(Paint.ANTI_ALIAS_FLAG));
                        d2code.recycle();

                    } catch (WriterException e) {
                        e.printStackTrace();
                    }

                    // 印外框
//                    Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
//                    p.setColor(Color.BLACK);
//                    p.setStrokeWidth(4);
//                    p.setStyle(Style.STROKE);
//                    c.drawRect(0, 0, 569, 244, p);

                    des.setType(IBluetoothPrint.TYPE_IMG);
                    des.setWidthP(1);
                    des.setHeightP(1);
                    bp.printBitmap(bm, des);
                    bm.recycle();

                    delayNextPrint = false;
                }
                else if (start == 2) {
                    // 印消費明細
                    des.setType(IBluetoothPrint.TYPE_TEXT);
                    des.setBlod(false);
                    des.setTextSize(IBluetoothPrint.TEXT_SIZE_B);
                    des.setGravity((byte) IBluetoothPrint.GRAVITY_CENTER);

                    bp.printText("---------------\n消費明細", des);
//                    bp.printText("", des);

                    delayNextPrint = false;
                }
                else if (start == 3) {
                    // 印消費明細
                    des.setType(IBluetoothPrint.TYPE_TEXT);
                    des.setBlod(false);
                    des.setTextSize(IBluetoothPrint.TEXT_SIZE_N);
                    des.setGravity((byte) IBluetoothPrint.GRAVITY_LEFT);

                    bp.printText("台北信義總店 TEL:(02)2621-5656", des);
                    bp.printText("營業人統編: 10718853", des);
                    bp.printText("2016/10/11 06:48:45", des);

                    bp.printText("24H光泉高鈣鮮豆漿      52T", des);
                    bp.printText("45 波的多蚵仔煎        16T", des);
                    bp.printText("45 波的多蚵仔煎        16T", des);
                    bp.printText("90 統一肉燥麵碗        19T", des);
                    bp.printText("68 統一肉骨茶碗        19T", des);

                    bp.printText("合計    5項    金額    $122", des);
                    bp.printText("現金   $200    找零     $78", des);
                    bp.printText("\n");

                    delayNextPrint = false;
                }
                else {
                    bp.printText("\n\n");
                    start = 0;

                    handler.removeCallbacks(this);
                    return;
                }

                start ++;
                if (delayNextPrint) {
                    // delay 1.5秒左右，避免前一張圖還沒印完
                    handler.postDelayed(this, 1500);
                }
                else {
                    handler.post(this);
                }
            }
        });

    }

    /** 產生電子發票範例圖片 */
    private Bitmap createFullSampleReceiptImage() {
        int maxWidth = 384; // 570
//        int maxHeight = 606; // 900
        int maxHeight = 383;
        // 0.67368421
        Bitmap bm = Bitmap.createBitmap(maxWidth, maxHeight, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bm);
        c.drawRGB(255, 255, 255);

        PathEffect pathEffect = new DashPathEffect(new float[]{10, 5, 10, 5}, 0);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setPathEffect(pathEffect);
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(4);
        paint.setStyle(Paint.Style.STROKE);

        Path path = new Path();
        path.moveTo(0, 0);
        path.lineTo(maxWidth, 0);
        c.drawPath(path, paint);

        Paint tp = new Paint(Paint.ANTI_ALIAS_FLAG);
        tp.setTextAlign(Paint.Align.CENTER);
        tp.setColor(Color.BLACK);

        tp.setTextSize(38);
        float offY=3;
        Paint.FontMetrics fm = tp.getFontMetrics();
        float fontTotalHeight = fm.bottom - fm.top;
        float offY1 = fontTotalHeight / 2 - fm.bottom;
        float newY = offY + fontTotalHeight/2 + offY1;

        String txt = "于江創意有限公司";
        c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

        offY = offY+fontTotalHeight-10;
        tp.setTextSize(43);
        tp.setFakeBoldText(false);
        fm = tp.getFontMetrics();
        fontTotalHeight = fm.bottom - fm.top;
        offY1 = fontTotalHeight / 2 - fm.bottom;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "電子發票證明聯補印";
        c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

        offY = offY+fontTotalHeight-10;
        tp.setTextSize(47);
        tp.setFakeBoldText(true);
        fm = tp.getFontMetrics();
        fontTotalHeight = fm.bottom - fm.top;
        offY1 = fontTotalHeight / 2 - fm.bottom;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "105年09-10月";
        c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

        offY = offY+fontTotalHeight-10;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "KT-28773338";
        c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

        offY = offY+fontTotalHeight-5;
        Log.e("TEST-Y-1", "offY="+offY); // 353.45117, 313.45117
        tp.setTextSize(24);
        tp.setFakeBoldText(false);
        tp.setTextAlign(Paint.Align.LEFT);
        fm = tp.getFontMetrics();
        fontTotalHeight = fm.bottom - fm.top;
        offY1 = fontTotalHeight / 2 - fm.bottom;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "2016/10/11 06:48:45";
        c.drawText(txt, 0, txt.length(), 20, newY, tp);

        offY = offY+fontTotalHeight-5;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "隨機碼: 2956";
        c.drawText(txt, 0, txt.length(), 20, newY, tp);

        txt = "總計: 990";
        c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

        offY = offY+fontTotalHeight-5;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "賣方: 10718853";
        c.drawText(txt, 0, txt.length(), 20, newY, tp);

        txt = "買方: 53796668";
        c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

        offY = offY+fontTotalHeight;
        Log.e("TEST-Y-2", "offY="+offY); //495.88672, 425.88672

        offY = offY+20;
        Bitmap d1code = ZXingCodeCreate.createOneDCode("10510LE422206938111", maxWidth, 80, false);
        c.drawBitmap(d1code,
                new Rect(0, 0, d1code.getWidth(), d1code.getHeight()),
                new Rect(0, (int)offY, maxWidth, (int)(offY+80)),
                new Paint(Paint.ANTI_ALIAS_FLAG));
        d1code.recycle();

        offY = offY+80;
        Log.e("TEST-Y-3", "offY="+offY); //595.8867, 525.8867
        /*
        int width = maxWidth/2;

        offY = offY+20;

//        int codeWidth = width/4;
        int codeWidth = 150;

        txt = "LE422206931050926811100000015000000160000000028426674yZ1zsLCd0XRVOg7VXOsI5A==:**********:5:5:1:24H光泉高鈣鮮豆漿:1:52:45 波的多蚵仔煎:1:16:";
        try {
            Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
            hints.put(EncodeHintType.MARGIN, 0);
            hints.put(EncodeHintType.DATA_MATRIX_SHAPE, SymbolShapeHint.FORCE_SQUARE);
            BitMatrix bitMatrix = new QRCodeWriter().encode(txt, BarcodeFormat.QR_CODE, codeWidth, codeWidth, hints);
            int[] pixels = new int[codeWidth*codeWidth];
            //下面這裡按照二維碼的算法，逐個生成二維碼的圖片，
            //兩個for循環是圖片橫列掃描的結果
            for (int y = 0; y < codeWidth; y++) {
                for (int x = 0; x < codeWidth; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * codeWidth + x] = 0xff000000;
                    }
                    else {
                        pixels[y * codeWidth + x] = 0xffffffff;
                    }
                }
            }

            Bitmap d2code = Bitmap.createBitmap(pixels, codeWidth, codeWidth, Bitmap.Config.RGB_565);
            c.drawBitmap(d2code,
                    new Rect(0, 0, d2code.getWidth(), d2code.getHeight()),
                    new Rect(10, (int) offY, width-10, (int) (offY+width-20)),
                    new Paint(Paint.ANTI_ALIAS_FLAG));
            d2code.recycle();

//            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
//            p.setColor(Color.BLACK);
//            p.setStrokeWidth(1);
//            p.setStyle(Paint.Style.STROKE);
//            c.drawRect(10, (int) offY, width-10, (int) (offY+width-20), p);

        } catch (WriterException e) {
            e.printStackTrace();
        }

        txt = "**45 波的多蚵仔煎:1:16:90 統一肉燥麵碗:1:19:68 統一肉骨茶碗:1:19";
//        txt = "**";
        try {
            Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
            hints.put(EncodeHintType.MARGIN, 0);
            hints.put(EncodeHintType.DATA_MATRIX_SHAPE, SymbolShapeHint.FORCE_SQUARE);
            BitMatrix bitMatrix = new QRCodeWriter().encode(txt, BarcodeFormat.QR_CODE, codeWidth, codeWidth, hints);
            int[] pixels = new int[codeWidth*codeWidth];
            //下面這裡按照二維碼的算法，逐個生成二維碼的圖片，
            //兩個for循環是圖片橫列掃描的結果
            for (int y = 0; y < codeWidth; y++) {
                for (int x = 0; x < codeWidth; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * codeWidth + x] = 0xff000000;
                    }
                    else {
                        pixels[y * codeWidth + x] = 0xffffffff;
                    }
                }
            }

            Bitmap d2code = Bitmap.createBitmap(pixels, codeWidth, codeWidth, Bitmap.Config.RGB_565);
            c.drawBitmap(d2code,
                    new Rect(0, 0, d2code.getWidth(), d2code.getHeight()),
                    new Rect(width+10, (int) offY, maxWidth-10, (int) (offY+width-20)),
                    new Paint(Paint.ANTI_ALIAS_FLAG));
            d2code.recycle();

//            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
//            p.setColor(Color.BLACK);
//            p.setStrokeWidth(1);
//            p.setStyle(Paint.Style.STROKE);
//            c.drawRect(width+10, (int) offY, maxWidth-10, (int) (offY+width-20), p);

        } catch (WriterException e) {
            e.printStackTrace();
        }

        offY = offY+width-20;
        Log.e("TEST-Y-4", "offY="+offY);

        PathEffect pathEffect = new DashPathEffect(new float[]{10, 5, 10, 5}, 0);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setPathEffect(pathEffect);
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(4);
        paint.setStyle(Paint.Style.STROKE);

        Path path = new Path();
        path.moveTo(0, c.getHeight()-5);
        path.lineTo(maxWidth, c.getHeight()-5);
        c.drawPath(path, paint);
        */

        return bm;
    }

    // --------------------------------------------------------------------


    /** 列印出菜單範例 (包括內, 外場) */
    private void printTestOrderredMenu() {
        PrintService.pl().connect("192.168.0.17");

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    if (PrintService.pl() != null) {
                        if (PrintService.pl().getState() == PrinterClass.STATE_CONNECTED) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    PrintService.pl().write(PrinterClass.CMD_BLOD_OFF);
//                            PrintService.pl().write(PrinterClass.CMD_SET_FONT_24x24);
                                    PrintService.pl().write(PrinterClass.CMD_FONTSIZE_DOUBLE);
                                    PrintService.pl().write(PrinterClass.CMD_ALIGN_MIDDLE);
                                    PrintService.pl().printText("---------------\r\n");
                                    PrintService.pl().printText("出菜單\r\n");

                                    PrintService.pl().write(PrinterClass.CMD_FONTSIZE_NORMAL);
                                    PrintService.pl().write(PrinterClass.CMD_ALIGN_LEFT);

                                    String txt = "時間 : 2016/11/08 14:03:15\r\n商業午餐\r\n碳烤土司    x1\r\n";
                                    PrintService.pl().printText(txt);

                                    PrintService.pl().write(PrinterClass.CMD_FONTSIZE_DOUBLE);
                                    PrintService.pl().write(PrinterClass.CMD_ALIGN_MIDDLE);
                                    PrintService.pl().printText("---------------\r\n");
                                    PrintService.pl().printText("出菜單\r\n");

                                    PrintService.pl().write(PrinterClass.CMD_FONTSIZE_NORMAL);
                                    PrintService.pl().write(PrinterClass.CMD_ALIGN_LEFT);

                                    txt = "時間 : 2016/11/08 14:03:15\r\n主餐\r\n茄汁香辣雞肉斜管麵    x1\r\n舒築特調脆薯    x1\r\n\r\n\r\n";
                                    PrintService.pl().printText(txt);

                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            PrintService.pl().disconnect();
                                        }
                                    }, 5000);
                                }
                            });
                            break;
                        } else if (PrintService.pl().getState() == PrinterClass.STATE_CONNECTING) {

                        } else if (PrintService.pl().getState() == PrinterClass.LOSE_CONNECT
                                || PrintService.pl().getState() == PrinterClass.FAILED_CONNECT) {
                            break;
                        } else {
                            break;
                        }
                    }
                }
            }
        });
        thread.start();

//        PrintService.pl().write(PrinterClass.CMD_BLOD_OFF);
////        PrintService.pl().write(PrinterClass.CMD_SET_FONT_24x24);
//        PrintService.pl().write(PrinterClass.CMD_FONTSIZE_DOUBLE);
//        PrintService.pl().write(PrinterClass.CMD_ALIGN_MIDDLE);
//        PrintService.pl().printText("---------------\r\n");
//        PrintService.pl().printText("出菜單\r\n");
//
//        PrintService.pl().write(PrinterClass.CMD_FONTSIZE_NORMAL);
//        PrintService.pl().write(PrinterClass.CMD_ALIGN_LEFT);
//
//        String txt = "時間 : 2016/11/08 14:03:15\r\n商業午餐\r\n碳烤土司    x1\r\n";
//        PrintService.pl().printText(txt);
//
//        PrintService.pl().write(PrinterClass.CMD_FONTSIZE_DOUBLE);
//        PrintService.pl().write(PrinterClass.CMD_ALIGN_MIDDLE);
//        PrintService.pl().printText("---------------\r\n");
//        PrintService.pl().printText("出菜單\r\n");
//
//        PrintService.pl().write(PrinterClass.CMD_FONTSIZE_NORMAL);
//        PrintService.pl().write(PrinterClass.CMD_ALIGN_LEFT);
//
//        txt = "時間 : 2016/11/08 14:03:15\r\n主餐\r\n茄汁香辣雞肉斜管麵    x1\r\n舒築特調脆薯    x1\r\n";
//        PrintService.pl().printText(txt);

    }

    /** 產生出菜單範例圖片 (isIn:內場 false:外場) */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    private Bitmap createSampleOrderredMenuImage(boolean isIn) {
        Bitmap bm = Bitmap.createBitmap(570, 900, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bm);
        c.drawRGB(255, 255, 255);

        Paint tp = new Paint(Paint.ANTI_ALIAS_FLAG);

        String tableName = "內用 桌號: 6";
        String txt;
        if (isIn) {
            // 內場

        }
        else {
            // 外場
            tp.setTextAlign(Paint.Align.CENTER);
            tp.setColor(Color.BLACK);
            tp.setFakeBoldText(true);

            tp.setTextSize(56);
            float offY = 10;
            Paint.FontMetrics fm = tp.getFontMetrics();
            float fontTotalHeight = fm.bottom - fm.top;
            float offY1 = fontTotalHeight / 2 - fm.bottom;
            float newY = offY + fontTotalHeight/2 + offY1;

            txt = "舒旅築旅";
            c.drawText(txt, 0, txt.length(), 570/2, newY, tp);

            offY = offY+fontTotalHeight;
            tp.setTextSize(50);
            tp.setFakeBoldText(false);
            fm = tp.getFontMetrics();
            fontTotalHeight = fm.bottom - fm.top;
            offY1 = fontTotalHeight / 2 - fm.bottom;
            newY = offY + fontTotalHeight/2 + offY1;

            tp.setTextAlign(Paint.Align.LEFT);
            c.drawText(tableName, 0, tableName.length(), 20, newY, tp);

            tp.setTextSize(40);
            tp.setFakeBoldText(false);
            tp.setTextAlign(Paint.Align.RIGHT);
            c.drawText("人數: 1", 0, 5, 550, newY, tp);

            offY = offY+fontTotalHeight;

            tp.setTextSize(34);
            tp.setFakeBoldText(false);
            tp.setTextAlign(Paint.Align.LEFT);
            fm = tp.getFontMetrics();
            fontTotalHeight = fm.bottom - fm.top;
            offY1 = fontTotalHeight / 2 - fm.bottom;
            newY = offY + fontTotalHeight/2 + offY1;

            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.TAIWAN);
            txt = format.format(new Date());
            c.drawText(txt, 0, txt.length(), 20, newY, tp);

            offY = offY+fontTotalHeight;

            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
            p.setARGB(255, 0, 0, 0);
            p.setStrokeWidth(2);
            p.setStyle(Paint.Style.STROKE);
            c.drawLine(20, offY, 550, offY, p);
            offY = offY+2;

            offY = offY+5;

            tp.setTextSize(42);
            tp.setFakeBoldText(false);
            tp.setTextAlign(Paint.Align.LEFT);
            fm = tp.getFontMetrics();
            fontTotalHeight = fm.bottom - fm.top;
            offY1 = fontTotalHeight / 2 - fm.bottom;
            newY = offY + fontTotalHeight/2 + offY1;

            txt = "和風鮭魚總匯沙拉";
            c.drawText(txt, 0, txt.length(), 20, newY, tp);

            tp.setTextAlign(Paint.Align.RIGHT);
            c.drawText("x2", 0, 2, 550, newY, tp);

            offY = offY+fontTotalHeight+5;
            newY = offY + fontTotalHeight/2 + offY1;

            tp.setTextAlign(Paint.Align.LEFT);
            txt = "舒築特選主廚湯";
            c.drawText(txt, 0, txt.length(), 20, newY, tp);

            tp.setTextAlign(Paint.Align.RIGHT);
            c.drawText("x1", 0, 2, 550, newY, tp);

            offY = offY+fontTotalHeight+5;
            newY = offY + fontTotalHeight/2 + offY1;

            tp.setTextAlign(Paint.Align.LEFT);
            txt = "舒築綜合海鮮湯";
            c.drawText(txt, 0, txt.length(), 20, newY, tp);

            tp.setTextAlign(Paint.Align.RIGHT);
            c.drawText("x1", 0, 2, 550, newY, tp);

            offY = offY+fontTotalHeight+5;
            newY = offY + fontTotalHeight/2 + offY1;

            tp.setTextAlign(Paint.Align.LEFT);
            txt = "彩椒雞肉紅醬義大利麵";
            c.drawText(txt, 0, txt.length(), 20, newY, tp);

            tp.setTextAlign(Paint.Align.RIGHT);
            c.drawText("x1", 0, 2, 550, newY, tp);

            offY = offY+fontTotalHeight+5;
            newY = offY + fontTotalHeight/2 + offY1;

            tp.setTextAlign(Paint.Align.LEFT);
            txt = "特製牛排 3分熟";
            c.drawText(txt, 0, txt.length(), 20, newY, tp);

            tp.setTextAlign(Paint.Align.RIGHT);
            c.drawText("x1", 0, 2, 550, newY, tp);

            offY = offY+fontTotalHeight+40;

//            c.drawLine(20, offY, 550, offY, p);
//            offY = offY+2;
//
//            offY = offY+5;

            tp.setTextAlign(Paint.Align.RIGHT);
            newY = 880 - fontTotalHeight/2 + offY1;
            txt = "總金額: 1220.00";
            c.drawText(txt, 0, txt.length(), 550, newY, tp);

            offY = 880-fontTotalHeight-5-2;
            c.drawLine(20, offY, 550, offY, p);

//            offY = offY+fontTotalHeight+10;

//            bm.reconfigure(bm.getWidth(), (int) offY, Bitmap.Config.RGB_565);
        }

        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
        p.setColor(Color.BLACK);
        p.setStrokeWidth(2);
        p.setStyle(Paint.Style.STROKE);
        c.drawRect(0, 0, bm.getWidth(), bm.getHeight(), p);

        return bm;
    }

}
