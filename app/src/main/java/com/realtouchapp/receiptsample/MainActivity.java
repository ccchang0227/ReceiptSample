package com.realtouchapp.receiptsample;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.os.StrictMode;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.smartdevice.sdk.printer.BlueToothService;
import com.smartdevice.sdk.printer.Device;
import com.smartdevice.sdk.printer.PrintService;
import com.smartdevice.sdk.printer.PrinterClass;
import com.wpx.WPXMain;
import com.wpx.util.WPXUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static com.smartdevice.sdk.printer.PrinterClass.MESSAGE_READ;
import static com.smartdevice.sdk.printer.PrinterClass.MESSAGE_STATE_CHANGE;
import static com.smartdevice.sdk.printer.PrinterClass.MESSAGE_WRITE;

public class MainActivity extends Activity {

    private View btn_open_bluetooth;
    private View btn_exit;

    private SwipeRefreshLayout srl_main;
    private RecyclerViewAdapter adapter;

    private List<BluetoothDevice> bondDevices = new ArrayList<BluetoothDevice>();
    private List<BluetoothDevice> unBondDevices = new ArrayList<BluetoothDevice>();
    private ArrayList<Object> list = new ArrayList<>();

    private Handler handler = new Handler();

    private Handler mhandler, xhandler;

    private ArrayList<Device> wifiPrinters = new ArrayList<Device>();

    private boolean checkData(List<Device> list, Device d) {
        for (Device device : list) {
            if (device.deviceAddress.equals(d.deviceAddress)) {
                return true;
            }
        }
        return false;
    }

    private void ShowMsg(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // -- wifi printer
        /**
         *
         */

        //  允许主线程连接网络
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mhandler = new Handler() {

            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MESSAGE_READ:
                        Log.e("AAAAA", "MESSAGE_READ:"+msg);

                        byte[] readBuf = (byte[]) msg.obj;
                        Log.e("MainActivity", "readBuf:" + readBuf[0]);
                        if (readBuf[0] == 0x13) {
                            PrintService.isFUll = true;
                            ShowMsg("State"
                                    + ":"
                                    + "Buffer Full");
                        } else if (readBuf[0] == 0x11) {
                            PrintService.isFUll = false;
                            ShowMsg("State"
                                    + ":"
                                    + "Buffer Null");
                        } else if (readBuf[0] == 0x08) {
                            ShowMsg("State"
                                    + ":"
                                    + "No Paper");
                        } else if (readBuf[0] == 0x01) {
                            // ShowMsg(getResources().getString(R.string.str_printer_state)+":"+getResources().getString(R.string.str_printer_printing));
                        } else if (readBuf[0] == 0x04) {
                            ShowMsg("State"
                                    + ":"
                                    + "High Temperature");
                        } else if (readBuf[0] == 0x02) {
                            ShowMsg("State"
                                    + ":"
                                    + "Low Power");
                        } else {
                            String readMessage = new String(readBuf, 0, msg.arg1);
                            if (readMessage.contains("800"))// 80mm paper
                            {
                                PrintService.imageWidth = 72;
                                Toast.makeText(getApplicationContext(), "80mm",
                                        Toast.LENGTH_SHORT).show();
                            } else if (readMessage.contains("580"))// 58mm paper
                            {
                                PrintService.imageWidth = 48;
                                Toast.makeText(getApplicationContext(), "58mm",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;
                    case MESSAGE_STATE_CHANGE:// 蓝牙连接状
                        Log.e("AAAAA", "MESSAGE_STATE_CHANGE:"+msg);
                        switch (msg.arg1) {
                            case PrinterClass.STATE_CONNECTED:// 已经连接
                                Toast.makeText(getApplicationContext(),
                                        "STATE_CONNECTED", Toast.LENGTH_SHORT).show();
                                break;
                            case PrinterClass.STATE_CONNECTING:// 正在连接
                                Toast.makeText(getApplicationContext(),
                                        "STATE_CONNECTING", Toast.LENGTH_SHORT).show();
                                break;
                            case PrinterClass.STATE_LISTEN:
                                Log.e("STATE_LISTEN", "STATE_LISTEN");
                            case PrinterClass.STATE_NONE:
                                Log.e("STATE_NONE", "STATE_NONE");
                                break;
                            case PrinterClass.SUCCESS_CONNECT:
                                PrintService.pl().write(new byte[] { 0x1b, 0x2b });// 检测打印机型号
                                Toast.makeText(getApplicationContext(),
                                        "SUCCESS_CONNECT", Toast.LENGTH_SHORT).show();
                                break;
                            case PrinterClass.FAILED_CONNECT:
                                Toast.makeText(getApplicationContext(),
                                        "FAILED_CONNECT", Toast.LENGTH_SHORT).show();
                                if (BlueToothService.autoConnect == 3) {
                                    // ???
                                } else {
                                    BlueToothService.reconnect();
                                }
                                break;
                            case PrinterClass.LOSE_CONNECT:
                                Toast.makeText(getApplicationContext(), "LOSE_CONNECT",
                                        Toast.LENGTH_SHORT).show();
                                break;
                        }
                        break;
                    case MESSAGE_WRITE:
                        Log.e("AAAAA", "MESSAGE_WRITE:"+msg);
                        break;
                }
                super.handleMessage(msg);
            }
        };

        xhandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 0:
                        break;
                    case 1:// 扫描完毕
                        Device d = (Device) msg.obj;
                        if (d != null) {

                            if (!checkData(wifiPrinters, d)) {
                                wifiPrinters.add(d);

                                reloadAll();
                            }
                        }
                        break;
                    case 2:// 停止扫描
                        break;
                }
            }
        };

        PrintService.PrinterInit(1, getApplicationContext(), mhandler, xhandler);

        // -- bluetooth printer

        // 藍牙搜尋callback
        WPXMain.addSerachDeviceCallBack(new WPXMain.SearchCallBack() {
            @Override
            public void startSearch() {

            }

            @Override
            public void searching(BluetoothDevice bluetoothDevice) {

            }

            @Override
            public void stopSearch() {
                srl_main.setRefreshing(false);

                reloadAll();
            }

            @Override
            public void onStateChange() {
                resetStateView();

                srl_main.setRefreshing(false);
            }
        });

        // -- setup layout

        reloadAll();
        initView();
        initRecyclerView();
    }

    @Override
    protected void onPause() {

        // wifi打印機停止搜尋
        PrintService.pl().stopScan();

        // 藍牙打印機停止搜尋
        WPXMain.stopSearchDevices();
        clearAll();
        srl_main.setRefreshing(false);

        super.onPause();
    }

    @Override
    protected void onResume() {

        // 有紀錄起來wifi 打印機的IP的話，可以直接呼叫connect()連接打印機
//        PrintService.pl().connect("192.168.0.17");

        super.onResume();
    }

    // 屏蔽返回键的代码 (智傑:我也不知道原sample為什麼要這麼做)
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void initView() {
        // 開啟/關閉藍牙
        btn_open_bluetooth = findViewById(R.id.btn_open_bluetooth);
        btn_open_bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (WPXMain.isBluetoothEnabled()) {
                    // 關閉
                    WPXMain.closeBluetooth();
                }
                else {
                    // 開啟
                    WPXMain.openBluetooth();

                    // delay 3秒後自動搜尋藍牙
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            resetStateView();
                            searchDevices();
                        }
                    }, 3000);
                }
            }
        });
        resetStateView();

        // 測試頁 (不需藍牙功能)
        View btn_test_page = findViewById(R.id.btn_test_page);
        btn_test_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(MainActivity.this, BluetoothDeviceActivity.class);
                intent.putExtra(BluetoothDeviceActivity.BUNDLE_EXTRA_BLUETOOTH_MODE_KEY, false);
                startActivity(intent);
            }
        });

        // 退出
        btn_exit = findViewById(R.id.btn_exit);
        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveTaskToBack(true);
                finish();

                Process.killProcess(Process.myPid());
                System.exit(0);
            }
        });

    }

    private void initRecyclerView() {
        // 下拉更新
        srl_main = (SwipeRefreshLayout) findViewById(R.id.srl_main);
        srl_main.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                // 搜尋藍牙裝置
                if (WPXMain.isBluetoothEnabled()) {
                    searchDevices();
                }
                else {
                    btn_open_bluetooth.performClick();
                    Toast.makeText(getApplicationContext(), "Bluetooth not open", Toast.LENGTH_SHORT).show();
                }

                // search wifi devices
                wifiPrinters.clear();
                if (!PrintService.pl().IsOpen()) {
                    PrintService.pl().open(getApplicationContext());
                }
                PrintService.pl().scan();
                if (PrintService.pl().getDeviceList() != null) {
                    wifiPrinters.addAll(PrintService.pl().getDeviceList());
                }

            }
        });
        srl_main.setRefreshing(false);

        RecyclerView rv_main = (RecyclerView) findViewById(R.id.rv_devices);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_main.setLayoutManager(layoutManager);
        rv_main.setLayoutManager(new LinearLayoutManager(this));//设置list布局
//        rv_main.setLayoutManager(new GridLayoutManager(getActivity(), 4));//设置网格布局
//        rv_main.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));//设置瀑布流布局

        adapter = new RecyclerViewAdapter(this, rv_main, list);
        // 點擊RecyclerView
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                final int index = position;
                Object device = list.get(position);
                if (device instanceof BluetoothDevice) {
                    final BluetoothDevice bt = (BluetoothDevice) device;
                    if (position < bondDevices.size()) {
                        // 開啟功能頁
                        final Intent intent = new Intent(MainActivity.this, BluetoothDeviceActivity.class);
                        intent.putExtra(BluetoothDeviceActivity.BUNDLE_EXTRA_BLUETOOTH_MODE_KEY, true);
                        intent.putExtra(BluetoothDeviceActivity.BUNDLE_EXTRA_DEVICE_KEY, bt);
                        startActivity(intent);
                    }
                    else {
                        // 裝置未綁定，先綁定裝置 (智傑:我也不知道原sample為什麼綁完沒自動連)
                        WPXUtils.bondDevice(bt);
                        handler.postDelayed(new Runnable(){
                            private int count = 0;
                            @Override
                            public void run() {
                                if (count++ >= 5) {
                                    handler.removeCallbacks(this);
                                }
                                else {
                                    if (bt.getBondState() == BluetoothDevice.BOND_BONDED) {
                                        reloadAll();
                                        handler.removeCallbacks(this);
                                    }
                                    else {
                                        handler.postDelayed(this, 1000);
                                    }
                                }
                            }
                        }, 1000);
                    }
                }
                else if (device instanceof Device) {
                    Device d = (Device) device;
                    PrintService.pl().connect(d.deviceAddress);
                }

            }
        });
        rv_main.setAdapter(adapter);
    }

    /**
     * 重置打开/关闭蓝牙按钮
     */
    private void resetStateView() {
        ((TextView) btn_open_bluetooth).setText(WPXMain.isBluetoothEnabled() ? R.string.close_bluetooth : R.string.open_bluetooth);
    }

    /**
     * 开始搜索蓝牙设备
     */
    private void searchDevices(){
        if (WPXMain.isBluetoothEnabled()) {
            WPXMain.startSearchDevices();
        }
    }
    /**
     * 重置绑定蓝牙设备到列表
     */
    private void resetBondDevice() {
        final Set<BluetoothDevice> set =  WPXMain.getBondedDevices();
        loadBondDevices(set);
//        final HashMap<String, BluetoothDevice> map = WPXMain.getSearchBondDevices();
//        loadBondDevices(map.values());
    }
    /**
     * 重置未绑定蓝牙设备到列表
     */
    private void resetUnBondDevice() {
        final HashMap<String, BluetoothDevice> map = WPXMain.getSearchUnBondDevices();
        loadUnbondDevices(map.values());
    }
    /**
     * 加载蓝牙设备到未绑定列表
     * @param devices
     */
    private void loadUnbondDevices(final Collection<BluetoothDevice> devices) {
        unBondDevices.clear();
        unBondDevices.addAll(devices);
    }
    /**
     * 加载蓝牙设备到绑定列表
     * @param devices
     */
    private void loadBondDevices(final Collection<BluetoothDevice> devices) {
        bondDevices.clear();
        bondDevices.addAll(devices);
    }

    // 重載藍牙裝置列表
    private void reloadAll() {
        list.clear();
        resetBondDevice();
        resetUnBondDevice();
        list.addAll(bondDevices);
        list.addAll(unBondDevices);
        list.addAll(wifiPrinters);

        if (adapter != null) {
            adapter.setItem(list);
        }
    }

    // 清空全部
    private void clearAll() {
        bondDevices.clear();
        unBondDevices.clear();

        list.clear();

        if (adapter != null) {
            adapter.setItem(list);
        }
    }

    // 點擊RecyclerView的callback
    public interface OnItemClickListener {
        public void onItemClick(int position, View v);
    }

    // RecyclerView的adapter
    private class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>
            implements View.OnClickListener {

        private ArrayList<Object> list = new ArrayList<>();

        private WeakReference<RecyclerView> delegate = null;
        private OnItemClickListener mItemClickListener = null;

        private Context context;
        private String sortKey = null;

        public RecyclerViewAdapter(Context context, RecyclerView rv, ArrayList<Object> list) {
            this.context = context;
            this.delegate = new WeakReference<RecyclerView>(rv);
            this.list = list;
        }

        public void setOnItemClickListener(OnItemClickListener listener) {
            this.mItemClickListener = listener;
        }

        @Override
        public int getItemViewType(int position) {
            //在Adapter中重写该方法，根据条件返回不同的值例如 0, 1, 2
//            return 0;

            return super.getItemViewType(position);
        }

        @Override
        public void onClick(View v) {
            int position =  delegate.get().getChildAdapterPosition(v);
            if (position != RecyclerView.NO_POSITION) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(position, v);
                }
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public View view;
            public TextView tv_name;
            public TextView tv_id;
            public TextView tv_state;
            public ViewHolder(View v) {
                super(v);
                view = v;
                v.setOnClickListener(RecyclerViewAdapter.this);

                tv_name = (TextView) v.findViewById(R.id.tv_name);

                tv_id = (TextView) v.findViewById(R.id.tv_id);

                tv_state = (TextView) v.findViewById(R.id.tv_state);
            }
        }

        @Override
        public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_rv_main, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int position) {
            viewHolder.view.setTag(position);
            int viewType = viewHolder.getItemViewType();

            final Object data = list.get(position);

            if (data != null) {
                if (data instanceof BluetoothDevice) {
                    // bluetooth
                    BluetoothDevice bt = (BluetoothDevice) data;
                    viewHolder.tv_name.setText(bt.getName());
                    viewHolder.tv_id.setText(bt.getAddress());

                    if (position < bondDevices.size()) {
                        viewHolder.tv_state.setText("已綁定");
                    }
                    else {
                        viewHolder.tv_state.setText("未綁定");
                    }
                }
                else if (data instanceof Device) {
                    // wifi
                    Device d = (Device) data;
                    viewHolder.tv_name.setText(d.deviceName);
                    viewHolder.tv_id.setText(d.deviceAddress);

                    viewHolder.tv_state.setText("Wifi");
                }
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public Object getItem(int position) {
            return list.get(position);
        }

        public void setItem(ArrayList<Object> list) {
            this.list = list;
        }
    }
}
